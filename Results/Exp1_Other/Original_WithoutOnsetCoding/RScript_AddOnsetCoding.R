#This Script adds onset coding to the first few originals in Group 1 that didn't have it.
library(tidyverse)
library(xlsx)

#Reading in coding file
setwd("Experiment 1/")
Coding=read.xlsx("List1.xlsx",sheetIndex=1) %>% 
  select(stimuli,Onset)

#Reading in data
setwd("../Results/Exp1_Group1/")
filenames = list.files(path="Original_WithoutOnsetCoding/",pattern="*.csv")

for(fileName in filenames){
  #fileName = filenames[1]
  AcceptabilitiesOriginal = read_csv(paste0("Original_WithoutOnsetCoding/",fileName)) %>% 
    left_join(Coding)
    
  write_csv(AcceptabilitiesOriginal,fileName)
}
