﻿/******************** 
 * Experiment1 Test *
 ********************/

import { PsychoJS } from './lib/core-2020.1.js';
import * as core from './lib/core-2020.1.js';
import { TrialHandler } from './lib/data-2020.1.js';
import { Scheduler } from './lib/util-2020.1.js';
import * as util from './lib/util-2020.1.js';
import * as visual from './lib/visual-2020.1.js';
import * as sound from './lib/sound-2020.1.js';

// init psychoJS:
const psychoJS = new PsychoJS({
  debug: true
});

// open window:
psychoJS.openWindow({
  fullscr: true,
  color: new util.Color([0, 0, 0]),
  units: 'height',
  waitBlanking: true
});

// store info about the experiment session:
let expName = 'Experiment1';  // from the Builder filename that created this script
let expInfo = {'participant': '', 'Age': '', 'Gender': '', 'NativeLanguage': '', 'Country of Residence': ''};

// schedule the experiment:
psychoJS.schedule(psychoJS.gui.DlgFromDict({
  dictionary: expInfo,
  title: expName
}));

const flowScheduler = new Scheduler(psychoJS);
const dialogCancelScheduler = new Scheduler(psychoJS);
psychoJS.scheduleCondition(function() { return (psychoJS.gui.dialogComponent.button === 'OK'); }, flowScheduler, dialogCancelScheduler);

// flowScheduler gets run if the participants presses OK
flowScheduler.add(updateInfo); // add timeStamp
flowScheduler.add(experimentInit);
flowScheduler.add(Consent1RoutineBegin());
flowScheduler.add(Consent1RoutineEachFrame());
flowScheduler.add(Consent1RoutineEnd());
flowScheduler.add(Consent2RoutineBegin());
flowScheduler.add(Consent2RoutineEachFrame());
flowScheduler.add(Consent2RoutineEnd());
flowScheduler.add(Consent3RoutineBegin());
flowScheduler.add(Consent3RoutineEachFrame());
flowScheduler.add(Consent3RoutineEnd());
flowScheduler.add(Consent4RoutineBegin());
flowScheduler.add(Consent4RoutineEachFrame());
flowScheduler.add(Consent4RoutineEnd());
flowScheduler.add(ActualConsentRoutineBegin());
flowScheduler.add(ActualConsentRoutineEachFrame());
flowScheduler.add(ActualConsentRoutineEnd());
flowScheduler.add(QuitOrContinueRoutineBegin());
flowScheduler.add(QuitOrContinueRoutineEachFrame());
flowScheduler.add(QuitOrContinueRoutineEnd());
flowScheduler.add(PreIntroRoutineBegin());
flowScheduler.add(PreIntroRoutineEachFrame());
flowScheduler.add(PreIntroRoutineEnd());
flowScheduler.add(IntroRoutineBegin());
flowScheduler.add(IntroRoutineEachFrame());
flowScheduler.add(IntroRoutineEnd());
flowScheduler.add(Practice_introRoutineBegin());
flowScheduler.add(Practice_introRoutineEachFrame());
flowScheduler.add(Practice_introRoutineEnd());
const practiceTrialsLoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(practiceTrialsLoopBegin, practiceTrialsLoopScheduler);
flowScheduler.add(practiceTrialsLoopScheduler);
flowScheduler.add(practiceTrialsLoopEnd);
flowScheduler.add(Test_introRoutineBegin());
flowScheduler.add(Test_introRoutineEachFrame());
flowScheduler.add(Test_introRoutineEnd());
const testTrialsLoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(testTrialsLoopBegin, testTrialsLoopScheduler);
flowScheduler.add(testTrialsLoopScheduler);
flowScheduler.add(testTrialsLoopEnd);
flowScheduler.add(ThanksRoutineBegin());
flowScheduler.add(ThanksRoutineEachFrame());
flowScheduler.add(ThanksRoutineEnd());
flowScheduler.add(quitPsychoJS, '', true);

// quit if user presses Cancel in dialog box:
dialogCancelScheduler.add(quitPsychoJS, '', false);

psychoJS.start({
  expName: expName,
  expInfo: expInfo,
  });


var frameDur;
function updateInfo() {
  expInfo['date'] = util.MonotonicClock.getDateStr();  // add a simple timestamp
  expInfo['expName'] = expName;
  expInfo['psychopyVersion'] = '2020.1.2';
  expInfo['OS'] = window.navigator.platform;

  // store frame rate of monitor if we can measure it successfully
  expInfo['frameRate'] = psychoJS.window.getActualFrameRate();
  if (typeof expInfo['frameRate'] !== 'undefined')
    frameDur = 1.0 / Math.round(expInfo['frameRate']);
  else
    frameDur = 1.0 / 60.0; // couldn't get a reliable measure so guess

  // add info from the URL:
  util.addInfoFromUrl(expInfo);
  psychoJS.setRedirectUrls('https://app.prolific.co/submissions/complete?cc=3E4D0117', '');

  return Scheduler.Event.NEXT;
}


var Consent1Clock;
var text_Consent1Prompt;
var key_resp_Consent1;
var text_Consent1Spacebar;
var Consent2Clock;
var text_Consent2Prompt;
var key_resp_Consent2;
var text_Consent2Spacebar;
var Consent3Clock;
var text_Consent3Prompt;
var key_resp_Consent3;
var text_Consent3Spacebar;
var Consent4Clock;
var text_Consent4Prompt;
var key_resp_2;
var text_4;
var ActualConsentClock;
var text_ActualConsentPrompt;
var ConsentResponse;
var QuitOrContinueClock;
var text_NoConsentBye;
var PreIntroClock;
var text_preIntro;
var text_3;
var key_resp;
var IntroClock;
var text_expIntro;
var text_expIntroSpacebar;
var key_expIntroSpacebar;
var Practice_introClock;
var text_practiceIntro;
var text_practicePressSpacebar;
var key_practiceIntroSpacebar;
var Stim_PresentationClock;
var text_listen;
var text_ratingPrompt;
var sound_1;
var Rating;
var Text_Input_RoutineClock;
var text_listen2;
var text_inputPrompt;
var PerceivedInput;
var polygon;
var Test_introClock;
var text_testIntro;
var text_testIntroSpacebar;
var key_testIntroSpacebar;
var ThanksClock;
var text_thanks;
var globalClock;
var routineTimer;
function experimentInit() {
  // Initialize components for Routine "Consent1"
  Consent1Clock = new util.Clock();
  text_Consent1Prompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent1Prompt',
    text: "You will take part in a research study about American English.\n\nPURPOSE: This is a research study on your native language (American English). From this study, the investigators hope to learn more about a speaker's knowledge of language. If you are under 18, you cannot be in this study.\n\nPROCEDURES: You will need headphones to participate in this study. You will listen to audio stimuli on your headphones and respond as prompted.",
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  key_resp_Consent1 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  text_Consent1Spacebar = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent1Spacebar',
    text: 'Hit Spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.4)], height: 0.025,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "Consent2"
  Consent2Clock = new util.Clock();
  text_Consent2Prompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent2Prompt',
    text: 'BENEFITS: You will not directly benefit from your participation in this study. However, your participation in this study may contribute to our understanding of the nature of linguistic knowledge.\n\nPOTENTIAL RISKS: There are no foreseeable risks associated with participation in this study.\n\nCONFIDENTIALITY: The data for this project will be kept in anonymised form. We do not collect any identifying information; we only collect general demographic details. The results of this study may be published or presented at professional meetings.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  key_resp_Consent2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  text_Consent2Spacebar = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent2Spacebar',
    text: 'Hit Spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.4)], height: 0.025,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "Consent3"
  Consent3Clock = new util.Clock();
  text_Consent3Prompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent3Prompt',
    text: 'FREEDOM TO WITHDRAW: Participation in this research project is completely voluntary. You have the right to not answer specific questions and withdraw participation at any time. Note, withdrawing participation will result in no payment.\n\nCOMPENSATION: You will be paid through Prolific for participation in this study.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  key_resp_Consent3 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  text_Consent3Spacebar = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent3Spacebar',
    text: 'Hit Spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.4)], height: 0.025,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "Consent4"
  Consent4Clock = new util.Clock();
  text_Consent4Prompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_Consent4Prompt',
    text: 'CONTACT: If you have concerns or questions about this study, please contact:\n\nDr. Karthik Durvasula\nAssistant Professor of Linguistics \nB-463 Wells Hall\nMichigan State University\nE-mail: karthikdurvasula@yahoo.com \nPhone number: 517-432-0194',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.04,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  key_resp_2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  text_4 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_4',
    text: 'Hit Spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.4)], height: 0.025,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "ActualConsent"
  ActualConsentClock = new util.Clock();
  text_ActualConsentPrompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_ActualConsentPrompt',
    text: 'Do you consent to the terms on the previous pages?\n\nPress "y" for yes; "n" for no',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.1], height: 0.04,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  ConsentResponse = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "QuitOrContinue"
  QuitOrContinueClock = new util.Clock();
  text_NoConsentBye = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_NoConsentBye',
    text: 'Since you have not given consent, you cannot continue with the experiment.\n\nThanks for your interest in the experiment! You will now exit the experiment.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  // Initialize components for Routine "PreIntro"
  PreIntroClock = new util.Clock();
  text_preIntro = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_preIntro',
    text: 'Please make sure you are in a quiet area, and can focus on the experiment.\n\nYou should now wear your headphones or earphones.\n\nMake sure that you pay careful attention to the instructions.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.2], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_3 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_3',
    text: 'Hit Spacebar when you are ready.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.3)], height: 0.025,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  key_resp = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Intro"
  IntroClock = new util.Clock();
  text_expIntro = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_expIntro',
    text: 'In the experiment that follows, you will hear a series of nonsense words.\n\nPlease rate how good you think each nonsense word would be as a possible word of English.\n\nUse the following scale:  1 = not English-like;  7 = English-like.\n\nYou will then be asked to type out the nonsense word that you heard.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.1], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_expIntroSpacebar = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_expIntroSpacebar',
    text: 'Hit spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.3)], height: 0.04,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  key_expIntroSpacebar = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Practice_intro"
  Practice_introClock = new util.Clock();
  text_practiceIntro = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_practiceIntro',
    text: 'First we will do a short practice round.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.05,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_practicePressSpacebar = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_practicePressSpacebar',
    text: 'Hit spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.3)], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  key_practiceIntroSpacebar = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Stim_Presentation"
  Stim_PresentationClock = new util.Clock();
  text_listen = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_listen',
    text: 'Listen carefully',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.3], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_ratingPrompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_ratingPrompt',
    text: 'How good a word of English is it on a scale of 1 - 7?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.1], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  sound_1 = new sound.Sound({
    win: psychoJS.window,
    value: 'A',
    secs: (- 1),
    });
  sound_1.setVolume(1);
  Rating = new visual.Slider({
    win: psychoJS.window, name: 'Rating',
    size: [0.7, 0.025], pos: [0, (- 0.2)], units: 'height',
    labels: ['not English-like', '2', '3', 4, '5', '6', 'English-like'], ticks: [1, 2, 3, 4, 5, 6, 7],
    granularity: 0, style: [visual.Slider.Style.RATING, visual.Slider.Style.RADIO],
    color: new util.Color('LightGray'), 
    fontFamily: 'HelveticaBold', bold: true, italic: false, 
    flip: false,
  });
  
  // Initialize components for Routine "Text_Input_Routine"
  Text_Input_RoutineClock = new util.Clock();
  text_listen2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_listen2',
    text: 'Listen carefully',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.3], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_inputPrompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_inputPrompt',
    text: 'Type what you heard, but please do not use punctuation\n(press Enter to finish)\n',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.1], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  PerceivedInput = new visual.TextStim({
    win: psychoJS.window,
    name: 'PerceivedInput',
    text: '',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.2)], height: 0.05,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  polygon = new visual.Rect ({
    win: psychoJS.window, name: 'polygon', 
    width: [0.5, 0.1][0], height: [0.5, 0.1][1],
    ori: 0, pos: [0, (- 0.2)],
    lineWidth: 0.01, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 0.3, depth: -4, interpolate: true,
  });
  
  // Initialize components for Routine "Test_intro"
  Test_introClock = new util.Clock();
  text_testIntro = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_testIntro',
    text: 'Now we will begin the actual experiment.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_testIntroSpacebar = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_testIntroSpacebar',
    text: 'Hit spacebar to continue.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.3)], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  key_testIntroSpacebar = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Stim_Presentation"
  Stim_PresentationClock = new util.Clock();
  text_listen = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_listen',
    text: 'Listen carefully',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.3], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_ratingPrompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_ratingPrompt',
    text: 'How good a word of English is it on a scale of 1 - 7?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.1], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  sound_1 = new sound.Sound({
    win: psychoJS.window,
    value: 'A',
    secs: (- 1),
    });
  sound_1.setVolume(1);
  Rating = new visual.Slider({
    win: psychoJS.window, name: 'Rating',
    size: [0.7, 0.025], pos: [0, (- 0.2)], units: 'height',
    labels: ['not English-like', '2', '3', 4, '5', '6', 'English-like'], ticks: [1, 2, 3, 4, 5, 6, 7],
    granularity: 0, style: [visual.Slider.Style.RATING, visual.Slider.Style.RADIO],
    color: new util.Color('LightGray'), 
    fontFamily: 'HelveticaBold', bold: true, italic: false, 
    flip: false,
  });
  
  // Initialize components for Routine "Text_Input_Routine"
  Text_Input_RoutineClock = new util.Clock();
  text_listen2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_listen2',
    text: 'Listen carefully',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.3], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  text_inputPrompt = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_inputPrompt',
    text: 'Type what you heard, but please do not use punctuation\n(press Enter to finish)\n',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0.1], height: 0.05,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  PerceivedInput = new visual.TextStim({
    win: psychoJS.window,
    name: 'PerceivedInput',
    text: '',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 0.2)], height: 0.05,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  polygon = new visual.Rect ({
    win: psychoJS.window, name: 'polygon', 
    width: [0.5, 0.1][0], height: [0.5, 0.1][1],
    ori: 0, pos: [0, (- 0.2)],
    lineWidth: 0.01, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 0.3, depth: -4, interpolate: true,
  });
  
  // Initialize components for Routine "Thanks"
  ThanksClock = new util.Clock();
  text_thanks = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_thanks',
    text: 'The experiment has ended.\n\nThank you for your participation!\n\nJust wait and you will be redirectly shortly so that you can confirm your participation on the prolific site.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 0.04,  wrapWidth: 1.5, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  // Create some handy timers
  globalClock = new util.Clock();  // to track the time since experiment started
  routineTimer = new util.CountdownTimer();  // to track time remaining of each (non-slip) routine
  
  return Scheduler.Event.NEXT;
}


var t;
var frameN;
var _key_resp_Consent1_allKeys;
var Consent1Components;
function Consent1RoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Consent1'-------
    t = 0;
    Consent1Clock.reset(); // clock
    frameN = -1;
    routineTimer.add(110.000000);
    // update component parameters for each repeat
    key_resp_Consent1.keys = undefined;
    key_resp_Consent1.rt = undefined;
    _key_resp_Consent1_allKeys = [];
    // keep track of which components have finished
    Consent1Components = [];
    Consent1Components.push(text_Consent1Prompt);
    Consent1Components.push(key_resp_Consent1);
    Consent1Components.push(text_Consent1Spacebar);
    
    for (const thisComponent of Consent1Components)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


var frameRemains;
var continueRoutine;
function Consent1RoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Consent1'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Consent1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_Consent1Prompt* updates
    if (t >= 0.0 && text_Consent1Prompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent1Prompt.tStart = t;  // (not accounting for frame time here)
      text_Consent1Prompt.frameNStart = frameN;  // exact frame index
      
      text_Consent1Prompt.setAutoDraw(true);
    }

    frameRemains = 0.0 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent1Prompt.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent1Prompt.setAutoDraw(false);
    }
    
    // *key_resp_Consent1* updates
    if (t >= 10 && key_resp_Consent1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_resp_Consent1.tStart = t;  // (not accounting for frame time here)
      key_resp_Consent1.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_resp_Consent1.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_resp_Consent1.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_resp_Consent1.clearEvents(); });
    }

    frameRemains = 10 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_resp_Consent1.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_resp_Consent1.status = PsychoJS.Status.FINISHED;
  }

    if (key_resp_Consent1.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_resp_Consent1.getKeys({keyList: ['space'], waitRelease: false});
      _key_resp_Consent1_allKeys = _key_resp_Consent1_allKeys.concat(theseKeys);
      if (_key_resp_Consent1_allKeys.length > 0) {
        key_resp_Consent1.keys = _key_resp_Consent1_allKeys[_key_resp_Consent1_allKeys.length - 1].name;  // just the last key pressed
        key_resp_Consent1.rt = _key_resp_Consent1_allKeys[_key_resp_Consent1_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    
    // *text_Consent1Spacebar* updates
    if (t >= 10 && text_Consent1Spacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent1Spacebar.tStart = t;  // (not accounting for frame time here)
      text_Consent1Spacebar.frameNStart = frameN;  // exact frame index
      
      text_Consent1Spacebar.setAutoDraw(true);
    }

    frameRemains = 10 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent1Spacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent1Spacebar.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Consent1Components)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Consent1RoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Consent1'-------
    for (const thisComponent of Consent1Components) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_resp_Consent1.keys', key_resp_Consent1.keys);
    if (typeof key_resp_Consent1.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_resp_Consent1.rt', key_resp_Consent1.rt);
        routineTimer.reset();
        }
    
    key_resp_Consent1.stop();
    return Scheduler.Event.NEXT;
  };
}


var _key_resp_Consent2_allKeys;
var Consent2Components;
function Consent2RoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Consent2'-------
    t = 0;
    Consent2Clock.reset(); // clock
    frameN = -1;
    routineTimer.add(110.000000);
    // update component parameters for each repeat
    key_resp_Consent2.keys = undefined;
    key_resp_Consent2.rt = undefined;
    _key_resp_Consent2_allKeys = [];
    // keep track of which components have finished
    Consent2Components = [];
    Consent2Components.push(text_Consent2Prompt);
    Consent2Components.push(key_resp_Consent2);
    Consent2Components.push(text_Consent2Spacebar);
    
    for (const thisComponent of Consent2Components)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function Consent2RoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Consent2'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Consent2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_Consent2Prompt* updates
    if (t >= 0.5 && text_Consent2Prompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent2Prompt.tStart = t;  // (not accounting for frame time here)
      text_Consent2Prompt.frameNStart = frameN;  // exact frame index
      
      text_Consent2Prompt.setAutoDraw(true);
    }

    frameRemains = 0.5 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent2Prompt.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent2Prompt.setAutoDraw(false);
    }
    
    // *key_resp_Consent2* updates
    if (t >= 10 && key_resp_Consent2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_resp_Consent2.tStart = t;  // (not accounting for frame time here)
      key_resp_Consent2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_resp_Consent2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_resp_Consent2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_resp_Consent2.clearEvents(); });
    }

    frameRemains = 10 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_resp_Consent2.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_resp_Consent2.status = PsychoJS.Status.FINISHED;
  }

    if (key_resp_Consent2.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_resp_Consent2.getKeys({keyList: ['space'], waitRelease: false});
      _key_resp_Consent2_allKeys = _key_resp_Consent2_allKeys.concat(theseKeys);
      if (_key_resp_Consent2_allKeys.length > 0) {
        key_resp_Consent2.keys = _key_resp_Consent2_allKeys[_key_resp_Consent2_allKeys.length - 1].name;  // just the last key pressed
        key_resp_Consent2.rt = _key_resp_Consent2_allKeys[_key_resp_Consent2_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    
    // *text_Consent2Spacebar* updates
    if (t >= 10 && text_Consent2Spacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent2Spacebar.tStart = t;  // (not accounting for frame time here)
      text_Consent2Spacebar.frameNStart = frameN;  // exact frame index
      
      text_Consent2Spacebar.setAutoDraw(true);
    }

    frameRemains = 10 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent2Spacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent2Spacebar.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Consent2Components)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Consent2RoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Consent2'-------
    for (const thisComponent of Consent2Components) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_resp_Consent2.keys', key_resp_Consent2.keys);
    if (typeof key_resp_Consent2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_resp_Consent2.rt', key_resp_Consent2.rt);
        routineTimer.reset();
        }
    
    key_resp_Consent2.stop();
    return Scheduler.Event.NEXT;
  };
}


var _key_resp_Consent3_allKeys;
var Consent3Components;
function Consent3RoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Consent3'-------
    t = 0;
    Consent3Clock.reset(); // clock
    frameN = -1;
    routineTimer.add(110.000000);
    // update component parameters for each repeat
    key_resp_Consent3.keys = undefined;
    key_resp_Consent3.rt = undefined;
    _key_resp_Consent3_allKeys = [];
    // keep track of which components have finished
    Consent3Components = [];
    Consent3Components.push(text_Consent3Prompt);
    Consent3Components.push(key_resp_Consent3);
    Consent3Components.push(text_Consent3Spacebar);
    
    for (const thisComponent of Consent3Components)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function Consent3RoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Consent3'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Consent3Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_Consent3Prompt* updates
    if (t >= 0.5 && text_Consent3Prompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent3Prompt.tStart = t;  // (not accounting for frame time here)
      text_Consent3Prompt.frameNStart = frameN;  // exact frame index
      
      text_Consent3Prompt.setAutoDraw(true);
    }

    frameRemains = 0.5 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent3Prompt.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent3Prompt.setAutoDraw(false);
    }
    
    // *key_resp_Consent3* updates
    if (t >= 10 && key_resp_Consent3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_resp_Consent3.tStart = t;  // (not accounting for frame time here)
      key_resp_Consent3.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_resp_Consent3.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_resp_Consent3.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_resp_Consent3.clearEvents(); });
    }

    frameRemains = 10 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_resp_Consent3.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_resp_Consent3.status = PsychoJS.Status.FINISHED;
  }

    if (key_resp_Consent3.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_resp_Consent3.getKeys({keyList: ['space'], waitRelease: false});
      _key_resp_Consent3_allKeys = _key_resp_Consent3_allKeys.concat(theseKeys);
      if (_key_resp_Consent3_allKeys.length > 0) {
        key_resp_Consent3.keys = _key_resp_Consent3_allKeys[_key_resp_Consent3_allKeys.length - 1].name;  // just the last key pressed
        key_resp_Consent3.rt = _key_resp_Consent3_allKeys[_key_resp_Consent3_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    
    // *text_Consent3Spacebar* updates
    if (t >= 10 && text_Consent3Spacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent3Spacebar.tStart = t;  // (not accounting for frame time here)
      text_Consent3Spacebar.frameNStart = frameN;  // exact frame index
      
      text_Consent3Spacebar.setAutoDraw(true);
    }

    frameRemains = 10 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent3Spacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent3Spacebar.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Consent3Components)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Consent3RoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Consent3'-------
    for (const thisComponent of Consent3Components) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_resp_Consent3.keys', key_resp_Consent3.keys);
    if (typeof key_resp_Consent3.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_resp_Consent3.rt', key_resp_Consent3.rt);
        routineTimer.reset();
        }
    
    key_resp_Consent3.stop();
    return Scheduler.Event.NEXT;
  };
}


var _key_resp_2_allKeys;
var Consent4Components;
function Consent4RoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Consent4'-------
    t = 0;
    Consent4Clock.reset(); // clock
    frameN = -1;
    routineTimer.add(102.000000);
    // update component parameters for each repeat
    key_resp_2.keys = undefined;
    key_resp_2.rt = undefined;
    _key_resp_2_allKeys = [];
    // keep track of which components have finished
    Consent4Components = [];
    Consent4Components.push(text_Consent4Prompt);
    Consent4Components.push(key_resp_2);
    Consent4Components.push(text_4);
    
    for (const thisComponent of Consent4Components)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function Consent4RoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Consent4'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Consent4Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_Consent4Prompt* updates
    if (t >= 0.5 && text_Consent4Prompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_Consent4Prompt.tStart = t;  // (not accounting for frame time here)
      text_Consent4Prompt.frameNStart = frameN;  // exact frame index
      
      text_Consent4Prompt.setAutoDraw(true);
    }

    frameRemains = 0.5 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_Consent4Prompt.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_Consent4Prompt.setAutoDraw(false);
    }
    
    // *key_resp_2* updates
    if (t >= 2 && key_resp_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_resp_2.tStart = t;  // (not accounting for frame time here)
      key_resp_2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_resp_2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_resp_2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_resp_2.clearEvents(); });
    }

    frameRemains = 2 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_resp_2.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_resp_2.status = PsychoJS.Status.FINISHED;
  }

    if (key_resp_2.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_resp_2.getKeys({keyList: ['space'], waitRelease: false});
      _key_resp_2_allKeys = _key_resp_2_allKeys.concat(theseKeys);
      if (_key_resp_2_allKeys.length > 0) {
        key_resp_2.keys = _key_resp_2_allKeys[_key_resp_2_allKeys.length - 1].name;  // just the last key pressed
        key_resp_2.rt = _key_resp_2_allKeys[_key_resp_2_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    
    // *text_4* updates
    if (t >= 2 && text_4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_4.tStart = t;  // (not accounting for frame time here)
      text_4.frameNStart = frameN;  // exact frame index
      
      text_4.setAutoDraw(true);
    }

    frameRemains = 2 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_4.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_4.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Consent4Components)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Consent4RoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Consent4'-------
    for (const thisComponent of Consent4Components) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_resp_2.keys', key_resp_2.keys);
    if (typeof key_resp_2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_resp_2.rt', key_resp_2.rt);
        routineTimer.reset();
        }
    
    key_resp_2.stop();
    return Scheduler.Event.NEXT;
  };
}


var _ConsentResponse_allKeys;
var ActualConsentComponents;
function ActualConsentRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'ActualConsent'-------
    t = 0;
    ActualConsentClock.reset(); // clock
    frameN = -1;
    // update component parameters for each repeat
    ConsentResponse.keys = undefined;
    ConsentResponse.rt = undefined;
    _ConsentResponse_allKeys = [];
    // keep track of which components have finished
    ActualConsentComponents = [];
    ActualConsentComponents.push(text_ActualConsentPrompt);
    ActualConsentComponents.push(ConsentResponse);
    
    for (const thisComponent of ActualConsentComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function ActualConsentRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'ActualConsent'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = ActualConsentClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_ActualConsentPrompt* updates
    if (t >= 0.5 && text_ActualConsentPrompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_ActualConsentPrompt.tStart = t;  // (not accounting for frame time here)
      text_ActualConsentPrompt.frameNStart = frameN;  // exact frame index
      
      text_ActualConsentPrompt.setAutoDraw(true);
    }

    
    // *ConsentResponse* updates
    if (t >= 0.5 && ConsentResponse.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      ConsentResponse.tStart = t;  // (not accounting for frame time here)
      ConsentResponse.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { ConsentResponse.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { ConsentResponse.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { ConsentResponse.clearEvents(); });
    }

    if (ConsentResponse.status === PsychoJS.Status.STARTED) {
      let theseKeys = ConsentResponse.getKeys({keyList: ['y', 'n'], waitRelease: false});
      _ConsentResponse_allKeys = _ConsentResponse_allKeys.concat(theseKeys);
      if (_ConsentResponse_allKeys.length > 0) {
        ConsentResponse.keys = _ConsentResponse_allKeys[_ConsentResponse_allKeys.length - 1].name;  // just the last key pressed
        ConsentResponse.rt = _ConsentResponse_allKeys[_ConsentResponse_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of ActualConsentComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function ActualConsentRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'ActualConsent'-------
    for (const thisComponent of ActualConsentComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('ConsentResponse.keys', ConsentResponse.keys);
    if (typeof ConsentResponse.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('ConsentResponse.rt', ConsentResponse.rt);
        routineTimer.reset();
        }
    
    ConsentResponse.stop();
    // the Routine "ActualConsent" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var startTime;
var QuitOrContinueComponents;
function QuitOrContinueRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'QuitOrContinue'-------
    t = 0;
    QuitOrContinueClock.reset(); // clock
    frameN = -1;
    routineTimer.add(100.500000);
    // update component parameters for each repeat
    startTime = globalClock.getTime();
    
    // keep track of which components have finished
    QuitOrContinueComponents = [];
    QuitOrContinueComponents.push(text_NoConsentBye);
    
    for (const thisComponent of QuitOrContinueComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function QuitOrContinueRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'QuitOrContinue'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = QuitOrContinueClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_NoConsentBye* updates
    if (t >= 0.5 && text_NoConsentBye.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_NoConsentBye.tStart = t;  // (not accounting for frame time here)
      text_NoConsentBye.frameNStart = frameN;  // exact frame index
      
      text_NoConsentBye.setAutoDraw(true);
    }

    frameRemains = 0.5 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_NoConsentBye.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_NoConsentBye.setAutoDraw(false);
    }
    if ((ConsentResponse.keys === "y")) {
        continueRoutine = false;
    } else {
        if (((globalClock.getTime() - startTime) >= 5)) {
            core.quit();
        }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of QuitOrContinueComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function QuitOrContinueRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'QuitOrContinue'-------
    for (const thisComponent of QuitOrContinueComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    return Scheduler.Event.NEXT;
  };
}


var _key_resp_allKeys;
var PreIntroComponents;
function PreIntroRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'PreIntro'-------
    t = 0;
    PreIntroClock.reset(); // clock
    frameN = -1;
    routineTimer.add(105.000000);
    // update component parameters for each repeat
    key_resp.keys = undefined;
    key_resp.rt = undefined;
    _key_resp_allKeys = [];
    // keep track of which components have finished
    PreIntroComponents = [];
    PreIntroComponents.push(text_preIntro);
    PreIntroComponents.push(text_3);
    PreIntroComponents.push(key_resp);
    
    for (const thisComponent of PreIntroComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function PreIntroRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'PreIntro'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = PreIntroClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_preIntro* updates
    if (t >= 0.5 && text_preIntro.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_preIntro.tStart = t;  // (not accounting for frame time here)
      text_preIntro.frameNStart = frameN;  // exact frame index
      
      text_preIntro.setAutoDraw(true);
    }

    frameRemains = 0.5 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_preIntro.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_preIntro.setAutoDraw(false);
    }
    
    // *text_3* updates
    if (t >= 10 && text_3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_3.tStart = t;  // (not accounting for frame time here)
      text_3.frameNStart = frameN;  // exact frame index
      
      text_3.setAutoDraw(true);
    }

    frameRemains = 10 + 95 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_3.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_3.setAutoDraw(false);
    }
    
    // *key_resp* updates
    if (t >= 10 && key_resp.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_resp.tStart = t;  // (not accounting for frame time here)
      key_resp.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_resp.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_resp.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_resp.clearEvents(); });
    }

    frameRemains = 10 + 95 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_resp.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_resp.status = PsychoJS.Status.FINISHED;
  }

    if (key_resp.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_resp.getKeys({keyList: ['space'], waitRelease: false});
      _key_resp_allKeys = _key_resp_allKeys.concat(theseKeys);
      if (_key_resp_allKeys.length > 0) {
        key_resp.keys = _key_resp_allKeys[_key_resp_allKeys.length - 1].name;  // just the last key pressed
        key_resp.rt = _key_resp_allKeys[_key_resp_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of PreIntroComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function PreIntroRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'PreIntro'-------
    for (const thisComponent of PreIntroComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_resp.keys', key_resp.keys);
    if (typeof key_resp.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_resp.rt', key_resp.rt);
        routineTimer.reset();
        }
    
    key_resp.stop();
    return Scheduler.Event.NEXT;
  };
}


var _key_expIntroSpacebar_allKeys;
var IntroComponents;
function IntroRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Intro'-------
    t = 0;
    IntroClock.reset(); // clock
    frameN = -1;
    routineTimer.add(100.000000);
    // update component parameters for each repeat
    key_expIntroSpacebar.keys = undefined;
    key_expIntroSpacebar.rt = undefined;
    _key_expIntroSpacebar_allKeys = [];
    // keep track of which components have finished
    IntroComponents = [];
    IntroComponents.push(text_expIntro);
    IntroComponents.push(text_expIntroSpacebar);
    IntroComponents.push(key_expIntroSpacebar);
    
    for (const thisComponent of IntroComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function IntroRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Intro'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = IntroClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_expIntro* updates
    if (t >= 0.0 && text_expIntro.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_expIntro.tStart = t;  // (not accounting for frame time here)
      text_expIntro.frameNStart = frameN;  // exact frame index
      
      text_expIntro.setAutoDraw(true);
    }

    frameRemains = 0.0 + 100 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_expIntro.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_expIntro.setAutoDraw(false);
    }
    
    // *text_expIntroSpacebar* updates
    if (t >= 8 && text_expIntroSpacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_expIntroSpacebar.tStart = t;  // (not accounting for frame time here)
      text_expIntroSpacebar.frameNStart = frameN;  // exact frame index
      
      text_expIntroSpacebar.setAutoDraw(true);
    }

    frameRemains = 8 + 92 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_expIntroSpacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_expIntroSpacebar.setAutoDraw(false);
    }
    
    // *key_expIntroSpacebar* updates
    if (t >= 8 && key_expIntroSpacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_expIntroSpacebar.tStart = t;  // (not accounting for frame time here)
      key_expIntroSpacebar.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_expIntroSpacebar.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_expIntroSpacebar.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_expIntroSpacebar.clearEvents(); });
    }

    frameRemains = 8 + 92 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_expIntroSpacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_expIntroSpacebar.status = PsychoJS.Status.FINISHED;
  }

    if (key_expIntroSpacebar.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_expIntroSpacebar.getKeys({keyList: ['space'], waitRelease: false});
      _key_expIntroSpacebar_allKeys = _key_expIntroSpacebar_allKeys.concat(theseKeys);
      if (_key_expIntroSpacebar_allKeys.length > 0) {
        key_expIntroSpacebar.keys = _key_expIntroSpacebar_allKeys[_key_expIntroSpacebar_allKeys.length - 1].name;  // just the last key pressed
        key_expIntroSpacebar.rt = _key_expIntroSpacebar_allKeys[_key_expIntroSpacebar_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of IntroComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function IntroRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Intro'-------
    for (const thisComponent of IntroComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_expIntroSpacebar.keys', key_expIntroSpacebar.keys);
    if (typeof key_expIntroSpacebar.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_expIntroSpacebar.rt', key_expIntroSpacebar.rt);
        routineTimer.reset();
        }
    
    key_expIntroSpacebar.stop();
    return Scheduler.Event.NEXT;
  };
}


var _key_practiceIntroSpacebar_allKeys;
var Practice_introComponents;
function Practice_introRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Practice_intro'-------
    t = 0;
    Practice_introClock.reset(); // clock
    frameN = -1;
    routineTimer.add(11.000000);
    // update component parameters for each repeat
    key_practiceIntroSpacebar.keys = undefined;
    key_practiceIntroSpacebar.rt = undefined;
    _key_practiceIntroSpacebar_allKeys = [];
    // keep track of which components have finished
    Practice_introComponents = [];
    Practice_introComponents.push(text_practiceIntro);
    Practice_introComponents.push(text_practicePressSpacebar);
    Practice_introComponents.push(key_practiceIntroSpacebar);
    
    for (const thisComponent of Practice_introComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function Practice_introRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Practice_intro'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Practice_introClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_practiceIntro* updates
    if (t >= 0.0 && text_practiceIntro.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_practiceIntro.tStart = t;  // (not accounting for frame time here)
      text_practiceIntro.frameNStart = frameN;  // exact frame index
      
      text_practiceIntro.setAutoDraw(true);
    }

    frameRemains = 0.0 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_practiceIntro.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_practiceIntro.setAutoDraw(false);
    }
    
    // *text_practicePressSpacebar* updates
    if (t >= 1 && text_practicePressSpacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_practicePressSpacebar.tStart = t;  // (not accounting for frame time here)
      text_practicePressSpacebar.frameNStart = frameN;  // exact frame index
      
      text_practicePressSpacebar.setAutoDraw(true);
    }

    frameRemains = 1 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_practicePressSpacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_practicePressSpacebar.setAutoDraw(false);
    }
    
    // *key_practiceIntroSpacebar* updates
    if (t >= 1 && key_practiceIntroSpacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_practiceIntroSpacebar.tStart = t;  // (not accounting for frame time here)
      key_practiceIntroSpacebar.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_practiceIntroSpacebar.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_practiceIntroSpacebar.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_practiceIntroSpacebar.clearEvents(); });
    }

    frameRemains = 1 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_practiceIntroSpacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_practiceIntroSpacebar.status = PsychoJS.Status.FINISHED;
  }

    if (key_practiceIntroSpacebar.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_practiceIntroSpacebar.getKeys({keyList: ['space'], waitRelease: false});
      _key_practiceIntroSpacebar_allKeys = _key_practiceIntroSpacebar_allKeys.concat(theseKeys);
      if (_key_practiceIntroSpacebar_allKeys.length > 0) {
        key_practiceIntroSpacebar.keys = _key_practiceIntroSpacebar_allKeys[_key_practiceIntroSpacebar_allKeys.length - 1].name;  // just the last key pressed
        key_practiceIntroSpacebar.rt = _key_practiceIntroSpacebar_allKeys[_key_practiceIntroSpacebar_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Practice_introComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Practice_introRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Practice_intro'-------
    for (const thisComponent of Practice_introComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_practiceIntroSpacebar.keys', key_practiceIntroSpacebar.keys);
    if (typeof key_practiceIntroSpacebar.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_practiceIntroSpacebar.rt', key_practiceIntroSpacebar.rt);
        routineTimer.reset();
        }
    
    key_practiceIntroSpacebar.stop();
    return Scheduler.Event.NEXT;
  };
}


var practiceTrials;
var currentLoop;
function practiceTrialsLoopBegin(thisScheduler) {
  // set up handler to look after randomisation of conditions etc
  practiceTrials = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 1, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: 'practice_stim.xlsx',
    seed: undefined, name: 'practiceTrials'
  });
  psychoJS.experiment.addLoop(practiceTrials); // add the loop to the experiment
  currentLoop = practiceTrials;  // we're now the current loop

  // Schedule all the trials in the trialList:
  for (const thisPracticeTrial of practiceTrials) {
    const snapshot = practiceTrials.getSnapshot();
    thisScheduler.add(importConditions(snapshot));
    thisScheduler.add(Stim_PresentationRoutineBegin(snapshot));
    thisScheduler.add(Stim_PresentationRoutineEachFrame(snapshot));
    thisScheduler.add(Stim_PresentationRoutineEnd(snapshot));
    thisScheduler.add(Text_Input_RoutineRoutineBegin(snapshot));
    thisScheduler.add(Text_Input_RoutineRoutineEachFrame(snapshot));
    thisScheduler.add(Text_Input_RoutineRoutineEnd(snapshot));
    thisScheduler.add(endLoopIteration(thisScheduler, snapshot));
  }

  return Scheduler.Event.NEXT;
}


function practiceTrialsLoopEnd() {
  psychoJS.experiment.removeLoop(practiceTrials);

  return Scheduler.Event.NEXT;
}


var testTrials;
function testTrialsLoopBegin(thisScheduler) {
  // set up handler to look after randomisation of conditions etc
  testTrials = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 1, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: 'List2.xlsx',
    seed: undefined, name: 'testTrials'
  });
  psychoJS.experiment.addLoop(testTrials); // add the loop to the experiment
  currentLoop = testTrials;  // we're now the current loop

  // Schedule all the trials in the trialList:
  for (const thisTestTrial of testTrials) {
    const snapshot = testTrials.getSnapshot();
    thisScheduler.add(importConditions(snapshot));
    thisScheduler.add(Stim_PresentationRoutineBegin(snapshot));
    thisScheduler.add(Stim_PresentationRoutineEachFrame(snapshot));
    thisScheduler.add(Stim_PresentationRoutineEnd(snapshot));
    thisScheduler.add(Text_Input_RoutineRoutineBegin(snapshot));
    thisScheduler.add(Text_Input_RoutineRoutineEachFrame(snapshot));
    thisScheduler.add(Text_Input_RoutineRoutineEnd(snapshot));
    thisScheduler.add(endLoopIteration(thisScheduler, snapshot));
  }

  return Scheduler.Event.NEXT;
}


function testTrialsLoopEnd() {
  psychoJS.experiment.removeLoop(testTrials);

  return Scheduler.Event.NEXT;
}


var Stim_PresentationComponents;
function Stim_PresentationRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Stim_Presentation'-------
    t = 0;
    Stim_PresentationClock.reset(); // clock
    frameN = -1;
    // update component parameters for each repeat
    sound_1 = new sound.Sound({
    win: psychoJS.window,
    value: Soundfile,
    secs: -1,
    });
    sound_1.setVolume(1);
    Rating.reset()
    // keep track of which components have finished
    Stim_PresentationComponents = [];
    Stim_PresentationComponents.push(text_listen);
    Stim_PresentationComponents.push(text_ratingPrompt);
    Stim_PresentationComponents.push(sound_1);
    Stim_PresentationComponents.push(Rating);
    
    for (const thisComponent of Stim_PresentationComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function Stim_PresentationRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Stim_Presentation'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Stim_PresentationClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_listen* updates
    if (t >= 0.0 && text_listen.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_listen.tStart = t;  // (not accounting for frame time here)
      text_listen.frameNStart = frameN;  // exact frame index
      
      text_listen.setAutoDraw(true);
    }

    frameRemains = 0.0 + 11.5 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_listen.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_listen.setAutoDraw(false);
    }
    
    // *text_ratingPrompt* updates
    if (t >= 0 && text_ratingPrompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_ratingPrompt.tStart = t;  // (not accounting for frame time here)
      text_ratingPrompt.frameNStart = frameN;  // exact frame index
      
      text_ratingPrompt.setAutoDraw(true);
    }

    frameRemains = 0 + 11.5 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_ratingPrompt.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_ratingPrompt.setAutoDraw(false);
    }
    // start/stop sound_1
    if (t >= 1.5 && sound_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      sound_1.tStart = t;  // (not accounting for frame time here)
      sound_1.frameNStart = frameN;  // exact frame index
      
      psychoJS.window.callOnFlip(function(){ sound_1.play(); });  // screen flip
      sound_1.status = PsychoJS.Status.STARTED;
    }
    if (t >= (sound_1.getDuration() + sound_1.tStart)     && sound_1.status === PsychoJS.Status.STARTED) {
      sound_1.stop();  // stop the sound (if longer than duration)
      sound_1.status = PsychoJS.Status.FINISHED;
    }
    
    // *Rating* updates
    if (t >= 1.5 && Rating.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      Rating.tStart = t;  // (not accounting for frame time here)
      Rating.frameNStart = frameN;  // exact frame index
      
      Rating.setAutoDraw(true);
    }

    frameRemains = 1.5 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (Rating.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      Rating.setAutoDraw(false);
    }
    
    // Check Rating for response to end routine
    if (Rating.getRating() !== undefined && Rating.status === PsychoJS.Status.STARTED) {
      continueRoutine = false; }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Stim_PresentationComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Stim_PresentationRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Stim_Presentation'-------
    for (const thisComponent of Stim_PresentationComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    sound_1.stop();  // ensure sound has stopped at end of routine
    psychoJS.experiment.addData('Rating.response', Rating.getRating());
    psychoJS.experiment.addData('Rating.rt', Rating.getRT());
    // the Routine "Stim_Presentation" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var modify;
var event;
var Text_Input_RoutineComponents;
function Text_Input_RoutineRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Text_Input_Routine'-------
    t = 0;
    Text_Input_RoutineClock.reset(); // clock
    frameN = -1;
    routineTimer.add(10.500000);
    // update component parameters for each repeat
    modify = false;
    PerceivedInput.text = "";
    event = psychoJS.eventManager;
    event.clearEvents("keyboard");
    
    // keep track of which components have finished
    Text_Input_RoutineComponents = [];
    Text_Input_RoutineComponents.push(text_listen2);
    Text_Input_RoutineComponents.push(text_inputPrompt);
    Text_Input_RoutineComponents.push(PerceivedInput);
    Text_Input_RoutineComponents.push(polygon);
    
    for (const thisComponent of Text_Input_RoutineComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


var _pj;
var keys;
function Text_Input_RoutineRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Text_Input_Routine'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Text_Input_RoutineClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_listen2* updates
    if (t >= 0 && text_listen2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_listen2.tStart = t;  // (not accounting for frame time here)
      text_listen2.frameNStart = frameN;  // exact frame index
      
      text_listen2.setAutoDraw(true);
    }

    frameRemains = 0 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_listen2.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_listen2.setAutoDraw(false);
    }
    
    // *text_inputPrompt* updates
    if (t >= 0.5 && text_inputPrompt.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_inputPrompt.tStart = t;  // (not accounting for frame time here)
      text_inputPrompt.frameNStart = frameN;  // exact frame index
      
      text_inputPrompt.setAutoDraw(true);
    }

    frameRemains = 0.5 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_inputPrompt.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_inputPrompt.setAutoDraw(false);
    }
    
    // *PerceivedInput* updates
    if (t >= 0.5 && PerceivedInput.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      PerceivedInput.tStart = t;  // (not accounting for frame time here)
      PerceivedInput.frameNStart = frameN;  // exact frame index
      
      PerceivedInput.setAutoDraw(true);
    }

    frameRemains = 0.5 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (PerceivedInput.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      PerceivedInput.setAutoDraw(false);
    }
    var _pj;
    function _pj_snippets(container) {
        function in_es6(left, right) {
            if (((right instanceof Array) || ((typeof right) === "string"))) {
                return (right.indexOf(left) > (- 1));
            } else {
                if (((right instanceof Map) || (right instanceof Set) || (right instanceof WeakMap) || (right instanceof WeakSet))) {
                    return right.has(left);
                } else {
                    return (left in right);
                }
            }
        }
        container["in_es6"] = in_es6;
        return container;
    }
    _pj = {};
    _pj_snippets(_pj);
    keys = event.getKeys();
    if (keys.length) {
        if (_pj.in_es6("space", keys)) {
            PerceivedInput.text = (PerceivedInput.text + " ");
        } else {
            if (_pj.in_es6("backspace", keys)) {
                PerceivedInput.text = PerceivedInput.text.slice(0, (- 1));
            } else {
                if ((_pj.in_es6("lshift", keys) || _pj.in_es6("rshift", keys))) {
                    modify = true;
                } else {
                    if (_pj.in_es6("return", keys)) {
                        continueRoutine = false;
                    } else {
                        if (modify) {
                            PerceivedInput.text = (PerceivedInput.text + keys[0].upper());
                            modify = false;
                        } else {
                            PerceivedInput.text = (PerceivedInput.text + keys[0]);
                        }
                    }
                }
            }
        }
    }
    
    
    // *polygon* updates
    if (t >= 0.5 && polygon.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      polygon.tStart = t;  // (not accounting for frame time here)
      polygon.frameNStart = frameN;  // exact frame index
      
      polygon.setAutoDraw(true);
    }

    frameRemains = 0.5 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (polygon.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      polygon.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Text_Input_RoutineComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Text_Input_RoutineRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Text_Input_Routine'-------
    for (const thisComponent of Text_Input_RoutineComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData("PerceivedInput", PerceivedInput.text);
    
    return Scheduler.Event.NEXT;
  };
}


var _key_testIntroSpacebar_allKeys;
var Test_introComponents;
function Test_introRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Test_intro'-------
    t = 0;
    Test_introClock.reset(); // clock
    frameN = -1;
    routineTimer.add(11.000000);
    // update component parameters for each repeat
    key_testIntroSpacebar.keys = undefined;
    key_testIntroSpacebar.rt = undefined;
    _key_testIntroSpacebar_allKeys = [];
    // keep track of which components have finished
    Test_introComponents = [];
    Test_introComponents.push(text_testIntro);
    Test_introComponents.push(text_testIntroSpacebar);
    Test_introComponents.push(key_testIntroSpacebar);
    
    for (const thisComponent of Test_introComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function Test_introRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Test_intro'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = Test_introClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_testIntro* updates
    if (t >= 0.0 && text_testIntro.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_testIntro.tStart = t;  // (not accounting for frame time here)
      text_testIntro.frameNStart = frameN;  // exact frame index
      
      text_testIntro.setAutoDraw(true);
    }

    frameRemains = 0.0 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_testIntro.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_testIntro.setAutoDraw(false);
    }
    
    // *text_testIntroSpacebar* updates
    if (t >= 1 && text_testIntroSpacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_testIntroSpacebar.tStart = t;  // (not accounting for frame time here)
      text_testIntroSpacebar.frameNStart = frameN;  // exact frame index
      
      text_testIntroSpacebar.setAutoDraw(true);
    }

    frameRemains = 1 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_testIntroSpacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_testIntroSpacebar.setAutoDraw(false);
    }
    
    // *key_testIntroSpacebar* updates
    if (t >= 1 && key_testIntroSpacebar.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      key_testIntroSpacebar.tStart = t;  // (not accounting for frame time here)
      key_testIntroSpacebar.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { key_testIntroSpacebar.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { key_testIntroSpacebar.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { key_testIntroSpacebar.clearEvents(); });
    }

    frameRemains = 1 + 10 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (key_testIntroSpacebar.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      key_testIntroSpacebar.status = PsychoJS.Status.FINISHED;
  }

    if (key_testIntroSpacebar.status === PsychoJS.Status.STARTED) {
      let theseKeys = key_testIntroSpacebar.getKeys({keyList: ['space'], waitRelease: false});
      _key_testIntroSpacebar_allKeys = _key_testIntroSpacebar_allKeys.concat(theseKeys);
      if (_key_testIntroSpacebar_allKeys.length > 0) {
        key_testIntroSpacebar.keys = _key_testIntroSpacebar_allKeys[_key_testIntroSpacebar_allKeys.length - 1].name;  // just the last key pressed
        key_testIntroSpacebar.rt = _key_testIntroSpacebar_allKeys[_key_testIntroSpacebar_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of Test_introComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function Test_introRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Test_intro'-------
    for (const thisComponent of Test_introComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    psychoJS.experiment.addData('key_testIntroSpacebar.keys', key_testIntroSpacebar.keys);
    if (typeof key_testIntroSpacebar.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('key_testIntroSpacebar.rt', key_testIntroSpacebar.rt);
        routineTimer.reset();
        }
    
    key_testIntroSpacebar.stop();
    return Scheduler.Event.NEXT;
  };
}


var ThanksComponents;
function ThanksRoutineBegin(trials) {
  return function () {
    //------Prepare to start Routine 'Thanks'-------
    t = 0;
    ThanksClock.reset(); // clock
    frameN = -1;
    routineTimer.add(5.000000);
    // update component parameters for each repeat
    // keep track of which components have finished
    ThanksComponents = [];
    ThanksComponents.push(text_thanks);
    
    for (const thisComponent of ThanksComponents)
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
    
    return Scheduler.Event.NEXT;
  };
}


function ThanksRoutineEachFrame(trials) {
  return function () {
    //------Loop for each frame of Routine 'Thanks'-------
    let continueRoutine = true; // until we're told otherwise
    // get current time
    t = ThanksClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_thanks* updates
    if (t >= 0.0 && text_thanks.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_thanks.tStart = t;  // (not accounting for frame time here)
      text_thanks.frameNStart = frameN;  // exact frame index
      
      text_thanks.setAutoDraw(true);
    }

    frameRemains = 0.0 + 5 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_thanks.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_thanks.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    for (const thisComponent of ThanksComponents)
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
        break;
      }
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function ThanksRoutineEnd(trials) {
  return function () {
    //------Ending Routine 'Thanks'-------
    for (const thisComponent of ThanksComponents) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    }
    return Scheduler.Event.NEXT;
  };
}


function endLoopIteration(thisScheduler, loop) {
  // ------Prepare for next entry------
  return function () {
    if (typeof loop !== 'undefined') {
      // ------Check if user ended loop early------
      if (loop.finished) {
        // Check for and save orphaned data
        if (psychoJS.experiment.isEntryEmpty()) {
          psychoJS.experiment.nextEntry(loop);
        }
      thisScheduler.stop();
      } else {
        const thisTrial = loop.getCurrentTrial();
        if (typeof thisTrial === 'undefined' || !('isTrials' in thisTrial) || thisTrial.isTrials) {
          psychoJS.experiment.nextEntry(loop);
        }
      }
    return Scheduler.Event.NEXT;
    }
  };
}


function importConditions(trials) {
  return function () {
    psychoJS.importAttributes(trials.getCurrentTrial());
    return Scheduler.Event.NEXT;
    };
}


function quitPsychoJS(message, isCompleted) {
  // Check for and save orphaned data
  if (psychoJS.experiment.isEntryEmpty()) {
    psychoJS.experiment.nextEntry();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  psychoJS.window.close();
  psychoJS.quit({message: message, isCompleted: isCompleted});
  
  return Scheduler.Event.QUIT;
}
