# Phonotactics Project

TO DO:
-add question about whether the participant used headphones or speakers to the end of the experiment
-fix the issue with the last slide not showing up so people can get paid
-add consent forms to psychopy experiemnt 

THINGS TO REMEMBER:
-some participants thought the [f]s were 'soft' and used 'ph' in the spelling (maybe something is weird with the f sounds?)
-come back to our idea to use the 1/2 vowel stimuli
-we should also run another version where all the participants hear all the stimuli vowel lenths 