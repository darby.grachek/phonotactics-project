#This script normalises amplitude.
# NOTE: IT DOESN'T DELETE THE FILES IN THE OBJECTS WINDOW
# THIS IS BECAUSE THE FILE NAMES WITH PERIODS CAUSE PROBLEMS FOR SELECTION.

clearinfo

#Type of audio recording
#change to "stimuli" (or) "practice" (or) "fillers"
typeOfAudio$ = "stimuli"

#Script in same directory as sound files.
directory$ = "original_audio/"+typeOfAudio$

#New sound files in the sub-directory
directory2$ = typeOfAudio$

Create Strings as file list... list 'directory$'/*.wav
numberOfFiles = Get number of strings

for ifile to numberOfFiles
	select Strings list
	fileName$ = Get string... ifile
	Read from file... 'directory$'/'fileName$'

	fileName$ = replace$("'fileName$'", ".wav", "",0)
	
	Convert to mono

	# Scaling average intensity to 70dB. you can change it to whatever level you want.
	Scale intensity... 70

	Save as WAV file... 'directory2$'/'fileName$'.wav

	#needs a hack to convert the periods to underscores in Praat
	#fileName_underscore$ = replace$(fileName$,".","_")
	#appendInfoLine: fileName_underscore$

	#selectObject: "Sound 'fileName$'"
	#plusObject: "Sound 'fileName$'_mono"
	#Remove
endfor


select Strings list
Remove
